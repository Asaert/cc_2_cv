// MCP4902/MCP4912/MCP4922 DAC
#include <SPI.h>
#include <Wire.h>

const int CS0 = 10; // Hard-coded pin 10 for chip select
const int CS1 = 6; // Hard-coded pin 10 for chip select
const int DIGITAL_OUT = 11;
const byte DAC_A = 0x00;
const byte DAC_B = 0x80;
const byte GAIN_1 = 0x20;
const byte GAIN_2 = 0x00;
const byte ACTIVE = 0x10;
const byte SHUTDOWN = 0x00;
 
const byte CC_PORT1 = 64;
const byte CC_PORT2 = 65;
const byte CC_PORT3 = 66;
const byte CC_PORT4 = 67;


int out_value;

void setup() 
{
  usbMIDI.setHandleControlChange (OnControlChange);
  pinMode(CS0, OUTPUT);
  pinMode(CS1, OUTPUT);
  pinMode(DIGITAL_OUT, OUTPUT);
  digitalWrite(DIGITAL_OUT, HIGH);
  digitalWrite(CS0, HIGH); 
  digitalWrite(CS1, HIGH); 
  SPI.begin();
}

void loop() 
{
  usbMIDI.read();
}

void OnControlChange (byte channel, byte control, byte value) 
{
  // Receive on all MIDI channels.
  switch (control)
  {
    case CC_PORT1:
      MCP4922_write(CS0, DAC_A, value << 5);
      break;
    case CC_PORT2:
      MCP4922_write(CS0, DAC_B, value << 5);
      break;
    case CC_PORT3:
      MCP4922_write(CS1, DAC_A, value << 5);
      break;
    case CC_PORT4:
      MCP4922_write(CS1, DAC_B, value << 5);
      break;
    default:;
  }
}

void MCP4922_write(int cs_pin, byte dac, int value) 
{
  digitalWrite(DIGITAL_OUT, HIGH);
  byte low = value & 0xff;
  byte high = (value >> 8) & 0x0f;
  digitalWrite(cs_pin, LOW);
  SPI.transfer(dac | GAIN_1 | ACTIVE | high);
  SPI.transfer(low);
  digitalWrite(cs_pin, HIGH);
  digitalWrite(DIGITAL_OUT, LOW); 
}



